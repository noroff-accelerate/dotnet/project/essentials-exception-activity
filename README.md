# C# Bank Account Exception Handling Activity

## Introduction
This activity involves creating a simple C# console application that simulates basic banking operations, emphasizing advanced exception handling techniques.

## Objectives
- Understand and implement custom exceptions.
- Apply `try`, `catch`, and `finally` blocks in exception handling.
- Demonstrate practical use of exception handling in a real-world scenario.

## Tasks
1. **BankAccount Class**: Implement a `BankAccount` class with properties for `AccountNumber` (string) and `Balance` (decimal). Include methods `Deposit(decimal amount)` and `Withdraw(decimal amount)`.

2. **Custom Exception**: Create a `InsufficientFundsException` class for handling situations where a withdrawal amount exceeds the account balance.

3. **Exception Handling in Withdraw Method**: Use `try-catch-finally` blocks in the `Withdraw` method. Catch both the `InsufficientFundsException` and general exceptions, and use `finally` to indicate transaction completion.

4. **Transaction Logging**: Add informative console messages in `Deposit` and `Withdraw` methods for transaction start, end, and any exceptions.

5. **Main Program Logic**: In the `Main` method, create an instance of `BankAccount`, and perform various deposit and withdrawal operations, including cases that trigger exceptions.

6. **Testing**: Ensure correct handling of both successful transactions and exceptions, reflecting typical banking operations.

## Requirements
- .NET SDK
- A text editor or IDE supporting C# (like Visual Studio)

## Running the Application
- Clone/download the provided starter code.
- Open the solution in your preferred IDE.
- Build and run the application.
- Follow the on-screen prompts to perform banking operations.

## Conclusion
This activity will help you understand how to effectively implement and handle exceptions in C#, crucial for building robust applications.
