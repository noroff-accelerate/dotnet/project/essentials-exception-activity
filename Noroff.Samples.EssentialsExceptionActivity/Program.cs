﻿namespace Noroff.Samples.EssentialsExceptionActivity
{
    using System;

    public class InsufficientFundsException : Exception
    {
        public InsufficientFundsException(string message) : base(message)
        {
        }
    }

    public class BankAccount
    {
        private decimal balance;

        public BankAccount(decimal initialBalance)
        {
            balance = initialBalance;
        }

        public void Withdraw(decimal amount)
        {
            try
            {
                if (amount > balance)
                {
                    throw new InsufficientFundsException("Insufficient funds for withdrawal");
                }
                balance -= amount;
                Console.WriteLine("Withdrawal successful");
            }
            catch (InsufficientFundsException e)
            {
                Console.WriteLine("Error: " + e.Message);
                throw;
            }
            catch (Exception e)
            {
                Console.WriteLine("An unexpected error occurred: " + e.Message);
            }
            finally
            {
                Console.WriteLine("Operation completed");
            }
        }
    }

    class Program
    {
        static void Main()
        {
            BankAccount account = new BankAccount(1000.00m); // Decimal literal

            try
            {
                account.Withdraw(1100.00m); // Attempt to withdraw more than the balance
            }
            catch (InsufficientFundsException e)
            {
                Console.WriteLine("Transaction failed: " + e.Message);
            }
        }
    }

}